FROM java:8-jdk-alpine
COPY ./target/speakio-0.0.1-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
EXPOSE 8081
CMD ["java","-jar","/usr/app/speakio-0.0.1-SNAPSHOT.jar"]