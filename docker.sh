
#!/usr/bin/env bash


# Exit immediately if a any command exits with a non-zero status

set -e


# Set destination branch

WAR=$1


echo "Test: $WAR"


chmod +x Dockerfile

docker build -t speakio/$WAR .

docker run --name localhost -e MYSQL_ROOT_PASSWORD= -e MYSQL_DATABASE=speakio -e MYSQL_USER=admin -e MYSQL_PASSWORD= -d mysql:5.6

docker run -p 8040:8081 --name $WAR -i speakio/$WAR

sleep 10

docker ps

echo "---"

docker ps -a
# if [ $WAR = "speakio" ]; then

#     docker exec -i speakio sh -c "curl -I http://172.17.0.1:8040/categories" > ret.txthttp

# fi

# head -n 1 ret.txthttp > ret2.txthttp

# sed -i 's/.\{9\}//' ret2.txthttp

# sed -i 's/ //' ret2.txthttp

# RETHTTP=$(cat ret2.txthttp)


# if [ $WAR = "speakio" ]; then

#     if [ ${RETHTTP:0:3} != "401" ]; then

#         echo "/categories -> ${RETHTTP:0:3} - NOT OK"

#         exit -1

#     else

#         echo "/categories -> ${RETHTTP:0:3} - OK"

#     fi

# fi
