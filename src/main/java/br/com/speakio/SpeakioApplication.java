package br.com.speakio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpeakioApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpeakioApplication.class, args);
	}

}
