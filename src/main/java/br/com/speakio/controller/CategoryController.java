package br.com.speakio.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.speakio.dto.CategoryDTO;
import br.com.speakio.mapper.CategoryMapper;
import br.com.speakio.model.Category;
import br.com.speakio.request.CategoryRequest;
import br.com.speakio.security.CheckSecurity;
import br.com.speakio.service.CategoryService;

@CrossOrigin
@RestController
@RequestMapping("categories")
public class CategoryController {

	@Autowired
	private CategoryService service;
	
	@Autowired
	private CategoryMapper mapper;
	
	@CheckSecurity.Category.GODMode
	@CheckSecurity.Category.OrganizerMode
	@GetMapping
	public List<CategoryDTO> listAll(){
		return service.listAll();
	}
	
	@CheckSecurity.Category.GODMode
	@CheckSecurity.Category.OrganizerMode
	@CheckSecurity.Category.UserMode
	@GetMapping("{id}")
	public ResponseEntity<?> getById(@PathVariable Long id) {
		
		Optional<Category> category = service.find(id);
		
		if (category.isPresent()) {
			return ResponseEntity.ok(mapper.modelToDTO(category.get()));
		}
		
		return ResponseEntity.notFound().build();
	
	}
	
	@CheckSecurity.Category.GODMode
	@PostMapping
	public ResponseEntity<?> save(@RequestBody @Valid CategoryRequest categoryRequest) {
		try {
			CategoryDTO categoryDTO = service.save(categoryRequest);			
			return ResponseEntity.status(HttpStatus.CREATED).body(categoryDTO);
		}catch(Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}		
	}
	
	@CheckSecurity.Category.GODMode
	@PutMapping("{id}")
	public ResponseEntity<?> update(@RequestBody @Valid CategoryRequest category, @PathVariable Long id) {
		
		Category categoryDB = service.find(id).orElse(null);
		
		if (categoryDB != null) {
			BeanUtils.copyProperties(category, categoryDB, "id");
			return ResponseEntity.ok(service.update(categoryDB));
		}	
			
		return ResponseEntity.notFound().build();
	}
	
	@CheckSecurity.Category.GODMode
	@DeleteMapping("{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			service.delete(id);	
			return ResponseEntity.noContent().build();
			
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
	}
	
}
