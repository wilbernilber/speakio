package br.com.speakio.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.speakio.dto.EventDTO;
import br.com.speakio.mapper.EventMapper;
import br.com.speakio.model.Event;
import br.com.speakio.request.EventRequest;
import br.com.speakio.security.CheckSecurity;
import br.com.speakio.service.EventService;

@CrossOrigin
@RestController
@RequestMapping("events")
public class EventController {

	@Autowired
	private EventService service;
	
	@Autowired
	private EventMapper mapper;
	
	@CheckSecurity.Event.GODMode
	@CheckSecurity.Event.OrganizerMode
	@GetMapping
	public List<EventDTO> listAll(){
		return service.listAll();
	}
	
	@CheckSecurity.Event.GODMode
	@CheckSecurity.Event.OrganizerMode
	@CheckSecurity.Event.UserMode
	@GetMapping("{id}")
	public ResponseEntity<?> getById(@PathVariable Long id) {
		
		Optional<Event> event = service.find(id);
		
		if (event.isPresent()) {
			return ResponseEntity.ok(mapper.modelToDTO(event.get()));
		}
		
		return ResponseEntity.notFound().build();
	
	}
	
	@CheckSecurity.Event.GODMode
	@PostMapping
	public ResponseEntity<?> save(@RequestBody @Valid EventRequest eventRequest) {
		try {
			EventDTO eventDTO = service.save(eventRequest);			
			return ResponseEntity.status(HttpStatus.CREATED).body(eventDTO);
		}catch(Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}		
	}
	
	@CheckSecurity.Event.GODMode
	@PutMapping("{id}")
	public ResponseEntity<?> update(@RequestBody @Valid EventRequest event, @PathVariable Long id) {
		
		Event eventDB = service.find(id).orElse(null);
		
		if (eventDB != null) {
			BeanUtils.copyProperties(event, eventDB, "id");
			return ResponseEntity.ok(service.update(eventDB));
		}	
			
		return ResponseEntity.notFound().build();
	}
	
	@CheckSecurity.Event.GODMode
	@DeleteMapping("{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			service.delete(id);	
			return ResponseEntity.noContent().build();
			
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
	}
	
}
