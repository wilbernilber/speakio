package br.com.speakio.controller;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import br.com.speakio.dto.PlataformDTO;
import br.com.speakio.mapper.PlataformMapper;
import br.com.speakio.model.Plataform;
import br.com.speakio.request.PlataformRequest;
import br.com.speakio.service.PlataformService;

@CrossOrigin
@RestController
@RequestMapping("plataform")
public class PlataformController {
	
	@Autowired
	private PlataformService service;
	
	@Autowired
	private PlataformMapper mapper;
	
	@GetMapping
	public List<PlataformDTO> listAll(){
		return service.listAll();
	}
	
	@GetMapping("{id}")
	public ResponseEntity<?> getById(@PathVariable Long id) {
		
		Optional<Plataform> category = service.find(id);
		
		if (category.isPresent()) {
			return ResponseEntity.ok(mapper.modelToDTO(category.get()));
		}
		
		return ResponseEntity.notFound().build();
	
	}
	
	@PostMapping
	public ResponseEntity<?> save(@RequestBody @Valid PlataformRequest plataformRequest) {	
		try {
			
			PlataformDTO plataformDTO = service.save(plataformRequest);			
			return ResponseEntity.status(HttpStatus.CREATED).body(plataformDTO);
		
		}catch(Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}		
	}
	
	@PutMapping("{id}")
	public ResponseEntity<?> update(@RequestBody @Valid PlataformRequest plataform, @PathVariable Long id) {
		
		Plataform plataformDB = service.find(id).orElse(null);
		
		if (plataformDB != null) {
			BeanUtils.copyProperties(plataform, plataformDB, "id");
			return ResponseEntity.ok(service.update(plataformDB));
		}	
			
		return ResponseEntity.notFound().build();
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			service.delete(id);	
			return ResponseEntity.noContent().build();
			
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
	}
	
}