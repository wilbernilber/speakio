package br.com.speakio.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.speakio.dto.UserDTO;
import br.com.speakio.mapper.UserMapper;
import br.com.speakio.model.User;
import br.com.speakio.request.UserRequest;
import br.com.speakio.security.CheckSecurity;
import br.com.speakio.service.UserService;

@CrossOrigin
@RestController
@RequestMapping("users")
public class UserController {

	@Autowired
	private UserService service;
	
	@Autowired
	private UserMapper mapper;
	
	@CheckSecurity.User.GODMode
	@CheckSecurity.User.OrganizerMode
	@GetMapping
	public List<UserDTO> listAll(){
		return service.listAll();
	}
	
	@CheckSecurity.User.GODMode
	@CheckSecurity.User.OrganizerMode
	@CheckSecurity.User.UserMode
	@GetMapping("{id}")
	public ResponseEntity<?> getById(@PathVariable Long id) {
		
		Optional<User> user = service.find(id);
		
		if (user.isPresent()) {
			return ResponseEntity.ok(mapper.modelToDTO(user.get()));
		}
		
		return ResponseEntity.notFound().build();
	
	}
	
	@CheckSecurity.User.GODMode
	@PostMapping
	public ResponseEntity<?> save(@RequestBody @Valid UserRequest userRequest) {
		try {
			UserDTO userDTO = service.save(userRequest);			
			return ResponseEntity.status(HttpStatus.CREATED).body(userDTO);
		}catch(Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}		
	}
	
	@CheckSecurity.User.GODMode
	@PutMapping("{id}")
	public ResponseEntity<?> update(@RequestBody @Valid UserRequest user, @PathVariable Long id) {
		
		User userDB = service.find(id).orElse(null);
		
		if (userDB != null) {
			BeanUtils.copyProperties(user, userDB, "id");
			return ResponseEntity.ok(service.update(userDB));
		}	
			
		return ResponseEntity.notFound().build();
	}
	
	@CheckSecurity.User.GODMode
	@DeleteMapping("{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			service.delete(id);	
			return ResponseEntity.noContent().build();
			
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
	}
	
}
