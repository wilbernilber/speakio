package br.com.speakio.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.speakio.dto.UserEventDTO;
import br.com.speakio.mapper.UserEventMapper;
import br.com.speakio.model.UserEvent;
import br.com.speakio.request.UserEventRequest;
import br.com.speakio.security.CheckSecurity;
import br.com.speakio.service.UserEventService;

@CrossOrigin
@RestController
@RequestMapping("userEvents")
public class UserEventController {

	@Autowired 
	private UserEventService service;
	
	@Autowired
	private UserEventMapper mapper;
	
	@CheckSecurity.UserEvent.GODMode
	@CheckSecurity.UserEvent.OrganizerMode
	@GetMapping
	public List<UserEventDTO> listAll(){
		return service.listAll();
	}
	
	@CheckSecurity.UserEvent.GODMode
	@CheckSecurity.UserEvent.OrganizerMode
	@CheckSecurity.UserEvent.UserMode
	@GetMapping("{id}")
	public ResponseEntity<?> getById(@PathVariable Long id) {
		
		Optional<UserEvent> userEvent = service.find(id);
		
		if (userEvent.isPresent()) {
			return ResponseEntity.ok(mapper.modelToDTO(userEvent.get()));
		}
		
		return ResponseEntity.notFound().build();
	
	}
	
	@CheckSecurity.UserEvent.GODMode
	@PostMapping
	public ResponseEntity<?> save(@RequestBody @Valid UserEventRequest userEventRequest) {
		try {
			UserEventDTO userEventDTO = service.save(userEventRequest);			
			return ResponseEntity.status(HttpStatus.CREATED).body(userEventDTO);
		}catch(Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}		
	}
	
	@CheckSecurity.UserEvent.GODMode
	@PutMapping("{id}")
	public ResponseEntity<?> update(@RequestBody @Valid UserEventRequest userEvent, @PathVariable Long id) {
		
		UserEvent userEventDB = service.find(id).orElse(null);
		
		if (userEventDB != null) {
			BeanUtils.copyProperties(userEvent, userEventDB, "id");
			return ResponseEntity.ok(service.update(userEventDB));
		}	
			
		return ResponseEntity.notFound().build();
	}
	
	@CheckSecurity.UserEvent.GODMode
	@DeleteMapping("{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			service.delete(id);	
			return ResponseEntity.noContent().build();
			
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
	}
	
}
