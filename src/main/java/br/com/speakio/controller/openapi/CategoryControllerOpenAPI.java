package br.com.speakio.controller.openapi;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import br.com.speakio.dto.CategoryDTO;
import br.com.speakio.exception.config.Problem;
import br.com.speakio.model.Category;
import br.com.speakio.request.CategoryRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Controller by Category")
public interface CategoryControllerOpenAPI {

	
	@ApiOperation("Register a category")
	@ApiResponses({ @ApiResponse(code = 201, message = "Registered Category", response = CategoryDTO.class) })	
	ResponseEntity<?> save(
			@ApiParam(name = "body", value = "Representation of a new category", required = true) 
			@Valid CategoryRequest categoryRequest);

	@ApiOperation(value = "Search all categories", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Search all categories", response = CategoryDTO.class) })
	List<CategoryDTO> listAll();
	
	@ApiOperation(value = "Search category by ID", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Search category by ID", response = CategoryDTO.class),
			@ApiResponse(code = 404, message = "The resource was not found", response = Problem.class) })
	@ApiImplicitParam(name = "id", value = "ID to fetch", required = true, dataType = "int", paramType = "path", example = "1")
	ResponseEntity<Category> getById(Long id);
	

	@ApiOperation(value = "Delete category pelo ID", httpMethod = "DELETE", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({ @ApiResponse(code = 204, message = "category successfully deleted", response = CategoryDTO.class),
			@ApiResponse(code = 404, message = "The resource was not found", response = Problem.class) })
	@ApiImplicitParam(name = "id", value = "ID to delete", required = true, dataType = "int", paramType = "path", example = "1")
	ResponseEntity<Category> excluir(Long id);
	

	@ApiOperation(value = "Update category by ID", httpMethod = "PUT", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({ @ApiResponse(code = 200, message = "Category updated successfully", response = CategoryDTO.class),
			@ApiResponse(code = 404, message = "The resource was not found", response = Problem.class) })
	@ApiImplicitParam(name = "id", value = "Id to update", required = true, dataType = "int", paramType = "path", example = "1")
	ResponseEntity<?> update(
			@ApiParam(name = "body", value = "Representation of a new category", required = true) @Valid Category category,
			Long id);

}
