package br.com.speakio.controller.openapi;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import br.com.speakio.dto.EventDTO;
import br.com.speakio.exception.config.Problem;
import br.com.speakio.model.Event;
import br.com.speakio.request.EventRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Controller by Event")
public interface EventControllerOpenAPI {

	
	@ApiOperation("Register a Event")
	@ApiResponses({ @ApiResponse(code = 201, message = "Registered Event", response = EventDTO.class) })	
	ResponseEntity<?> save(
			@ApiParam(name = "body", value = "Representation of a new Event", required = true) 
			@Valid EventRequest eventRequest);

	@ApiOperation(value = "Search all categories", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Search all categories", response = EventDTO.class) })
	List<EventDTO> listAll();
	
	@ApiOperation(value = "Search Event by ID", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Search Event by ID", response = EventDTO.class),
			@ApiResponse(code = 404, message = "The resource was not found", response = Problem.class) })
	@ApiImplicitParam(name = "id", value = "ID to fetch", required = true, dataType = "int", paramType = "path", example = "1")
	ResponseEntity<Event> getById(Long id);
	

	@ApiOperation(value = "Delete Event pelo ID", httpMethod = "DELETE", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({ @ApiResponse(code = 204, message = "Event successfully deleted", response = EventDTO.class),
			@ApiResponse(code = 404, message = "The resource was not found", response = Problem.class) })
	@ApiImplicitParam(name = "id", value = "ID to delete", required = true, dataType = "int", paramType = "path", example = "1")
	ResponseEntity<Event> excluir(Long id);
	

	@ApiOperation(value = "Update Event by ID", httpMethod = "PUT", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({ @ApiResponse(code = 200, message = "Event updated successfully", response = EventDTO.class),
			@ApiResponse(code = 404, message = "The resource was not found", response = Problem.class) })
	@ApiImplicitParam(name = "id", value = "Id to update", required = true, dataType = "int", paramType = "path", example = "1")
	ResponseEntity<?> update(
			@ApiParam(name = "body", value = "Representation of a new Event", required = true) @Valid Event event,
			Long id);

}
