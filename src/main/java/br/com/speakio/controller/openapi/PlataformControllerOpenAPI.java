package br.com.speakio.controller.openapi;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import br.com.speakio.dto.PlataformDTO;
import br.com.speakio.exception.config.Problem;
import br.com.speakio.model.Plataform;
import br.com.speakio.request.PlataformRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Controller by Plataform")
public interface PlataformControllerOpenAPI {

	
	@ApiOperation("Register a Plataform")
	@ApiResponses({ @ApiResponse(code = 201, message = "Registered Plataform", response = PlataformDTO.class) })	
	ResponseEntity<?> save(
			@ApiParam(name = "body", value = "Representation of a new Plataform", required = true) 
			@Valid PlataformRequest plataformRequest);

	@ApiOperation(value = "Search all categories", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Search all categories", response = PlataformDTO.class) })
	List<PlataformDTO> listAll();
	
	@ApiOperation(value = "Search Plataform by ID", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Search Plataform by ID", response = PlataformDTO.class),
			@ApiResponse(code = 404, message = "The resource was not found", response = Problem.class) })
	@ApiImplicitParam(name = "id", value = "ID to fetch", required = true, dataType = "int", paramType = "path", example = "1")
	ResponseEntity<Plataform> getById(Long id);
	

	@ApiOperation(value = "Delete Plataform pelo ID", httpMethod = "DELETE", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({ @ApiResponse(code = 204, message = "Plataform successfully deleted", response = PlataformDTO.class),
			@ApiResponse(code = 404, message = "The resource was not found", response = Problem.class) })
	@ApiImplicitParam(name = "id", value = "ID to delete", required = true, dataType = "int", paramType = "path", example = "1")
	ResponseEntity<Plataform> excluir(Long id);
	

	@ApiOperation(value = "Update Plataform by ID", httpMethod = "PUT", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({ @ApiResponse(code = 200, message = "Plataform updated successfully", response = PlataformDTO.class),
			@ApiResponse(code = 404, message = "The resource was not found", response = Problem.class) })
	@ApiImplicitParam(name = "id", value = "Id to update", required = true, dataType = "int", paramType = "path", example = "1")
	ResponseEntity<?> update(
			@ApiParam(name = "body", value = "Representation of a new Plataform", required = true) @Valid Plataform plataform,
			Long id);

}
