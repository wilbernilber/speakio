package br.com.speakio.controller.openapi;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;

import br.com.speakio.exception.config.Problem;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Controller by Token")
public interface TokenControllerOpenAPI {

	@ApiOperation(value = "Delete token", httpMethod = "DELETE", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({ @ApiResponse(code = 204, message = "token successfully deleted"),
			@ApiResponse(code = 404, message = "The resource was not found", response = Problem.class) })
	public void revoke(HttpServletRequest req, HttpServletResponse resp);

}
