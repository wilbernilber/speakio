package br.com.speakio.controller.openapi;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import br.com.speakio.dto.UserDTO;
import br.com.speakio.exception.config.Problem;
import br.com.speakio.model.User;
import br.com.speakio.request.UserRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Controller by User")
public interface UserControllerOpenAPI {

	
	@ApiOperation("Register a User")
	@ApiResponses({ @ApiResponse(code = 201, message = "Registered User", response = UserDTO.class) })	
	ResponseEntity<?> save(
			@ApiParam(name = "body", value = "Representation of a new User", required = true) 
			@Valid UserRequest userRequest);

	@ApiOperation(value = "Search all categories", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Search all categories", response = UserDTO.class) })
	List<UserDTO> listAll();
	
	@ApiOperation(value = "Search User by ID", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Search User by ID", response = UserDTO.class),
			@ApiResponse(code = 404, message = "The resource was not found", response = Problem.class) })
	@ApiImplicitParam(name = "id", value = "ID to fetch", required = true, dataType = "int", paramType = "path", example = "1")
	ResponseEntity<User> getById(Long id);
	

	@ApiOperation(value = "Delete User pelo ID", httpMethod = "DELETE", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({ @ApiResponse(code = 204, message = "User successfully deleted", response = UserDTO.class),
			@ApiResponse(code = 404, message = "The resource was not found", response = Problem.class) })
	@ApiImplicitParam(name = "id", value = "ID to delete", required = true, dataType = "int", paramType = "path", example = "1")
	ResponseEntity<User> excluir(Long id);
	

	@ApiOperation(value = "Update User by ID", httpMethod = "PUT", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({ @ApiResponse(code = 200, message = "User updated successfully", response = UserDTO.class),
			@ApiResponse(code = 404, message = "The resource was not found", response = Problem.class) })
	@ApiImplicitParam(name = "id", value = "Id to update", required = true, dataType = "int", paramType = "path", example = "1")
	ResponseEntity<?> update(
			@ApiParam(name = "body", value = "Representation of a new User", required = true) @Valid User user,
			Long id);

}
