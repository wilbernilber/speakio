package br.com.speakio.controller.openapi;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import br.com.speakio.dto.UserEventDTO;
import br.com.speakio.exception.config.Problem;
import br.com.speakio.model.UserEvent;
import br.com.speakio.request.UserEventRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Controller by UserEvent")
public interface UserEventControllerOpenAPI {

	
	@ApiOperation("Register a UserEvent")
	@ApiResponses({ @ApiResponse(code = 201, message = "Registered UserEvent", response = UserEventDTO.class) })	
	ResponseEntity<?> save(
			@ApiParam(name = "body", value = "Representation of a new UserEvent", required = true) 
			@Valid UserEventRequest userEventRequest);

	@ApiOperation(value = "Search all categories", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Search all categories", response = UserEventDTO.class) })
	List<UserEventDTO> listAll();
	
	@ApiOperation(value = "Search UserEvent by ID", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Search UserEvent by ID", response = UserEventDTO.class),
			@ApiResponse(code = 404, message = "The resource was not found", response = Problem.class) })
	@ApiImplicitParam(name = "id", value = "ID to fetch", required = true, dataType = "int", paramType = "path", example = "1")
	ResponseEntity<UserEvent> getById(Long id);
	

	@ApiOperation(value = "Delete UserEvent pelo ID", httpMethod = "DELETE", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({ @ApiResponse(code = 204, message = "UserEvent successfully deleted", response = UserEventDTO.class),
			@ApiResponse(code = 404, message = "The resource was not found", response = Problem.class) })
	@ApiImplicitParam(name = "id", value = "ID to delete", required = true, dataType = "int", paramType = "path", example = "1")
	ResponseEntity<UserEvent> excluir(Long id);
	

	@ApiOperation(value = "Update UserEvent by ID", httpMethod = "PUT", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({ @ApiResponse(code = 200, message = "UserEvent updated successfully", response = UserEventDTO.class),
			@ApiResponse(code = 404, message = "The resource was not found", response = Problem.class) })
	@ApiImplicitParam(name = "id", value = "Id to update", required = true, dataType = "int", paramType = "path", example = "1")
	ResponseEntity<?> update(
			@ApiParam(name = "body", value = "Representation of a new UserEvent", required = true) @Valid UserEvent userEvent,
			Long id);

}
