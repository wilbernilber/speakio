package br.com.speakio.dto;

import br.com.speakio.model.Event;
import br.com.speakio.model.User;
import lombok.Data;

@Data
public class EventDTO {
	private Long id;	
    User userEvent;
    Event event;
}
