package br.com.speakio.dto;

import lombok.Data;

@Data
public class PlataformDTO {
	private Long id;
	private String name;
}
