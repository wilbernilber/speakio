package br.com.speakio.dto;

import br.com.speakio.model.Event;
import br.com.speakio.model.User;
import lombok.Data;

@Data
public class UserEventDTO {

	private Long id;	
	private User user;
    private Event event;
	
}
