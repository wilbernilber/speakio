package br.com.speakio.exception;

public class EventNotFoundException extends EntityNotFoundException{

	private static final long serialVersionUID = 1L;

	public EventNotFoundException(String message) {
		super(message);		
	}

	public EventNotFoundException(Long id) {
		this(String.format("There is no event registration with code %d", id));
	}
}
