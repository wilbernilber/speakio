package br.com.speakio.exception;

public class OperationNotFoundException extends EntityNotFoundException{

	private static final long serialVersionUID = 1L;

	public OperationNotFoundException(String message) {
		super(message);		
	}

	public OperationNotFoundException(Long id) {
		this(String.format("There is no operation registration with code %d", id));
	}
}
