package br.com.speakio.exception;

public class PlataformNotFoundException extends EntityNotFoundException{

	private static final long serialVersionUID = 1L;

	public PlataformNotFoundException(String message) {
		super(message);		
	}

	public PlataformNotFoundException(Long id) {
		this(String.format("There is no customer registration with code %d", id));
	}
}
