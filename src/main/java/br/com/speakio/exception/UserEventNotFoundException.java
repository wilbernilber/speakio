package br.com.speakio.exception;

public class UserEventNotFoundException extends EntityNotFoundException{

	private static final long serialVersionUID = 1L;

	public UserEventNotFoundException(String message) {
		super(message);		
	}

	public UserEventNotFoundException(Long id) {
		this(String.format("There is no user event registration with code %d", id));
	}
}
