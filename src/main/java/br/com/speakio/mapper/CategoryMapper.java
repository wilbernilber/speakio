package br.com.speakio.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.speakio.dto.CategoryDTO;
import br.com.speakio.model.Category;
import br.com.speakio.request.CategoryRequest;

@Component
public class CategoryMapper {

	@Autowired
	private ModelMapper modelMapper;

	public Category requestToModel(CategoryRequest categoryRequest) {
		return modelMapper.map(categoryRequest, Category.class);
	}

	public CategoryDTO modelToDTO(Category category) {
		return modelMapper.map(category, CategoryDTO.class);
	}
}
