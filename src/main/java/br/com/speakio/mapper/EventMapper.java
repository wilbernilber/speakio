package br.com.speakio.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.speakio.dto.EventDTO;
import br.com.speakio.model.Event;
import br.com.speakio.request.EventRequest;

@Component
public class EventMapper {

	@Autowired
	private ModelMapper modelMapper;

	public Event requestToModel(EventRequest eventRequest) {
		return modelMapper.map(eventRequest, Event.class);
	}

	public EventDTO modelToDTO(Event event) {
		return modelMapper.map(event, EventDTO.class);
	}
}