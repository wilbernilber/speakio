package br.com.speakio.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.speakio.dto.PlataformDTO;
import br.com.speakio.model.Plataform;
import br.com.speakio.request.PlataformRequest;

@Component
public class PlataformMapper {

	@Autowired
	private ModelMapper modelMapper;

	public Plataform requestToModel(PlataformRequest plataformRequest) {
		return modelMapper.map(plataformRequest, Plataform.class);
	}

	public PlataformDTO modelToDTO(Plataform plataform) {
		return modelMapper.map(plataform, PlataformDTO.class);
	}
}