package br.com.speakio.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.speakio.dto.UserEventDTO;
import br.com.speakio.model.UserEvent;
import br.com.speakio.request.UserEventRequest;

@Component
public class UserEventMapper {

	@Autowired
	private ModelMapper modelMapper;

	public UserEvent requestToModel(UserEventRequest userEventRequest) {
		return modelMapper.map(userEventRequest, UserEvent.class);
	}

	public UserEventDTO modelToDTO(UserEvent userEvent) {
		return modelMapper.map(userEvent, UserEventDTO.class);
	}
}
