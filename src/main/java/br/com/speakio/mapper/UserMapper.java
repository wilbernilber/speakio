package br.com.speakio.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.speakio.dto.UserDTO;
import br.com.speakio.model.User;
import br.com.speakio.request.UserRequest;

@Component
public class UserMapper {

	@Autowired
	private ModelMapper modelMapper;

	public User requestToModel(UserRequest userRequest) {
		return modelMapper.map(userRequest, User.class);
	}

	public UserDTO modelToDTO(User user) {
		return modelMapper.map(user, UserDTO.class);
	}
}