package br.com.speakio.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
@Entity
@Table(name="event")
public class Event {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;	
	
	@ManyToOne
	@JoinColumn(name="user_id", nullable = false) 
	private User user;	
	
	@OneToMany(mappedBy = "event")
    private List<UserEvent> userEvents;
	
	@Column(nullable = false, length = 80)
	private String name;	
	
	@Column(name = "number_of_vancancies", nullable = false)
	private Integer numberOfVacancies;
	
	@DateTimeFormat(pattern="yyyy-mm-dd")
	@Column(name="date")
	private LocalDate date;	
	
	@Column(name="start_time", nullable = false, length = 5)
	private String startTime;	
	
	@Column(name="end_time", nullable = false, length = 5)
	private String endTime;	
	
	@Column(nullable = false, length = 256)
	private String description;
	
	@ManyToOne
	@JoinColumn(name="category_id", nullable = false)
	private Category category;
	
	@ManyToOne
	@JoinColumn(name="plataform_id", nullable = false)
	private Plataform plataform;
	
	@Column(nullable = false, length = 80)
	private String url;

}
