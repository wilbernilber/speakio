package br.com.speakio.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name="user")
public class User {

	@Id
	@EqualsAndHashCode.Include
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;	
	
	@Column(nullable = false, length = 80)
	private String name;	
	
	@Column(name="nick_name", nullable = false, length = 30)
	private String nickName;
	
	@Column(nullable = false, length = 61)
	@JsonIgnore	
	private String password;		
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private List<Event> events;
	
	@OneToMany(mappedBy = "userEvent")
    private List<UserEvent> userEvents;
	
	@Column(nullable = false, length = 80)
	private String email;
	
	@Column(nullable = false, length = 15)
	private String access;
	
	@ManyToMany
	@JoinTable(name = "user_group", joinColumns = @JoinColumn(name = "user_id"),
			inverseJoinColumns = @JoinColumn(name = "group_id"))
	private Set<GroupUser> groups = new HashSet<>();
	
}
