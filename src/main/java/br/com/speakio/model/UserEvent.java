package br.com.speakio.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name="user_event")
public class UserEvent {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;	
	
	@JsonIgnore
	@ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    User userEvent;
 
	@JsonIgnore
    @ManyToOne
    @JoinColumn(name = "event_id")
    Event event;
}
