package br.com.speakio.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.speakio.model.Category;
import br.com.speakio.model.Event;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
	@Query("select events from Category c where c.id = :id")
	List<Event> findEventById(Long id);
}
