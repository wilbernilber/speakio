package br.com.speakio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.speakio.model.Event;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

}
