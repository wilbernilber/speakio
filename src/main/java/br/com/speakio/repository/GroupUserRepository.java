package br.com.speakio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

import br.com.speakio.model.GroupUser;

@Repository
public interface GroupUserRepository extends JpaRepository<GroupUser, Long>{
	
	@Query(value = "SELECT * FROM groupuser WHERE name = ?1", nativeQuery = true)
	public Set<GroupUser> findGroup(String permission);
}
