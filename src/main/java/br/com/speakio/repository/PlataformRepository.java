package br.com.speakio.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.speakio.model.Event;
import br.com.speakio.model.Plataform;

@Repository
public interface PlataformRepository extends JpaRepository<Plataform, Long> {
	@Query("select events from Plataform p where p.id = :id")
	List<Event> findEventById(Long id);
}
