package br.com.speakio.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.speakio.model.Event;
import br.com.speakio.model.UserEvent;

@Repository
public interface UserEventRepository extends JpaRepository<UserEvent, Long> {
	@Query("select events from User u where u.id = :id")
	List<Event> findEventByIdUser(Long id);
}
