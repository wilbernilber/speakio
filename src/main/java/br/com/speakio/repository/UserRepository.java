package br.com.speakio.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.speakio.model.Event;
import br.com.speakio.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	@Query("select events from User u where u.id = :id")
	List<Event> findEventById(Long id);
	
	Optional<User> findByEmail(String email);
	
	Optional<User> findByNickName(String nickName);
}
