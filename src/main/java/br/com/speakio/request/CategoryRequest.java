package br.com.speakio.request;

import lombok.Data;

@Data
public class CategoryRequest {

	private Long id;
	private String name;
	
}
