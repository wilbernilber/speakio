package br.com.speakio.request;

import java.time.LocalDate;

import br.com.speakio.model.Category;
import br.com.speakio.model.Plataform;
import br.com.speakio.model.User;
import lombok.Data;

@Data
public class EventRequest {

	private Long id;
	private User user;
	private Integer numberOfVacancies;	
	private LocalDate date;		
	private String startTime;	
	private String endTime;	
	private String description;
	private Category category;
	private Plataform plataform;
	private String url;
	
}
