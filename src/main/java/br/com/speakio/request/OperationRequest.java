package br.com.speakio.request;

import lombok.Data;

@Data
public class OperationRequest {

	private Long id;
	private String name;
	
}
