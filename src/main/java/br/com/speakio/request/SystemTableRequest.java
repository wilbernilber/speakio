package br.com.speakio.request;

import lombok.Data;

@Data
public class SystemTableRequest {

	private Long id;
	private String name;
	
}
