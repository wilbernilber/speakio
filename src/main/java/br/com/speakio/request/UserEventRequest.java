package br.com.speakio.request;

import br.com.speakio.model.Event;
import br.com.speakio.model.User;
import lombok.Data;

@Data
public class UserEventRequest {

	User user;
    Event event;
	
}
