package br.com.speakio.request;

import javax.validation.constraints.Email;

import lombok.Data;

@Data
public class UserRequest {

	private Long id;
	private String name;
	private String nickName;
	private String password;
	private Integer access;
	@Email
	private String email;
	
}
