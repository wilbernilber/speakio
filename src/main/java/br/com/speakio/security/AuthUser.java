package br.com.speakio.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import lombok.Getter;

@Getter
public class AuthUser extends User {

private static final long serialVersionUID = 1L;
	
	private Long userId;
	private String name;
	
	public AuthUser(br.com.speakio.model.User user, Collection<? extends GrantedAuthority> permissions) {
		super(user.getNickName(), user.getPassword(), permissions);
		
		this.userId = user.getId();
		this.name = user.getName();
	}
	
}
