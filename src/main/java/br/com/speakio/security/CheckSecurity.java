package br.com.speakio.security;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.security.access.prepost.PreAuthorize;

public @interface CheckSecurity {

	public @interface Category {

		// SPEAKIO01
		@PreAuthorize("isAuthenticated() and " + 
				"hasAuthority('SPEAKIO01')")
		@Retention(RUNTIME)
		@Target(METHOD)
		public @interface GODMode {
		}
		
		// SPEAKIO02
		@PreAuthorize("isAuthenticated() and " + 
				"hasAuthority('SPEAKIO02')")
		@Retention(RUNTIME)
		@Target(METHOD)
		public @interface OrganizerMode {
		}
		
		// SPEAKIO03
		@PreAuthorize("isAuthenticated() and " + 
				"hasAuthority('SPEAKIO03')")
		@Retention(RUNTIME)
		@Target(METHOD)
		public @interface UserMode {
		}
	}

	public @interface Plataform {

		// SPEAKIO01
		@PreAuthorize("isAuthenticated() and " + 
				"hasAuthority('SPEAKIO01')")
		@Retention(RUNTIME)
		@Target(METHOD)
		public @interface GODMode {
		}
		
		// SPEAKIO02
		@PreAuthorize("isAuthenticated() and " + 
				"hasAuthority('SPEAKIO02')")
		@Retention(RUNTIME)
		@Target(METHOD)
		public @interface OrganizerMode {
		}
		
		// SPEAKIO03
		@PreAuthorize("isAuthenticated() and " + 
				"hasAuthority('SPEAKIO03')")
		@Retention(RUNTIME)
		@Target(METHOD)
		public @interface UserMode {
		}
	}
	
	public @interface User {

		// SPEAKIO01
		@PreAuthorize("isAuthenticated() and " + 
				"hasAuthority('SPEAKIO01')")
		@Retention(RUNTIME)
		@Target(METHOD)
		public @interface GODMode {
		}
		
		// SPEAKIO02
		@PreAuthorize("isAuthenticated() and " + 
				"hasAuthority('SPEAKIO02')")
		@Retention(RUNTIME)
		@Target(METHOD)
		public @interface OrganizerMode {
		}
		
		// SPEAKIO03
		@PreAuthorize("isAuthenticated() and " + 
				"hasAuthority('SPEAKIO03')")
		@Retention(RUNTIME)
		@Target(METHOD)
		public @interface UserMode {
		}
	}
	
	public @interface Event {

		// SPEAKIO01
		@PreAuthorize("isAuthenticated() and " + 
				"hasAuthority('SPEAKIO01')")
		@Retention(RUNTIME)
		@Target(METHOD)
		public @interface GODMode {
		}
		
		// SPEAKIO02
		@PreAuthorize("isAuthenticated() and " + 
				"hasAuthority('SPEAKIO02')")
		@Retention(RUNTIME)
		@Target(METHOD)
		public @interface OrganizerMode {
		}
		
		// SPEAKIO03
		@PreAuthorize("isAuthenticated() and " + 
				"hasAuthority('SPEAKIO03')")
		@Retention(RUNTIME)
		@Target(METHOD)
		public @interface UserMode {
		}
	}
	
	public @interface UserEvent {

		// SPEAKIO01
		@PreAuthorize("isAuthenticated() and " + 
				"hasAuthority('SPEAKIO01')")
		@Retention(RUNTIME)
		@Target(METHOD)
		public @interface GODMode {
		}
		
		// SPEAKIO02
		@PreAuthorize("isAuthenticated() and " + 
				"hasAuthority('SPEAKIO02')")
		@Retention(RUNTIME)
		@Target(METHOD)
		public @interface OrganizerMode {
		}
		
		// SPEAKIO03
		@PreAuthorize("isAuthenticated() and " + 
				"hasAuthority('SPEAKIO03')")
		@Retention(RUNTIME)
		@Target(METHOD)
		public @interface UserMode {
		}
	}

	
}