package br.com.speakio.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.speakio.dto.CategoryDTO;
import br.com.speakio.exception.CategoryNotFoundException;
import br.com.speakio.mapper.CategoryMapper;
import br.com.speakio.model.Category;
import br.com.speakio.repository.CategoryRepository;
import br.com.speakio.request.CategoryRequest;

@Service
public class CategoryService {

	@Autowired
	private CategoryRepository repository;
	@Autowired
	private CategoryMapper mapper;

	@Transactional
	public CategoryDTO save(CategoryRequest categoryRequest) {
		Category category = mapper.requestToModel(categoryRequest);
		//cliente.setDate(LocalDate.now());
	    return mapper.modelToDTO( repository.save(category) );		
	}

	@Transactional
	public CategoryDTO update(Category category) {
		return mapper.modelToDTO( repository.save(category) );		
	}
	
	public Optional<Category> find(Long id) {
		return repository.findById(id);
	}

	@Transactional
	public void delete(Long id) {
		
		try {
			repository.deleteById(id);
			repository.flush();
		
		} catch (EmptyResultDataAccessException e) {
			throw new CategoryNotFoundException(id);
		};			
	}

	public List<CategoryDTO> listAll() {
		
		return repository.findAll()
				.stream()
				.map(category -> mapper.modelToDTO(category))
				.collect(Collectors.toList());	
	}
	
}
