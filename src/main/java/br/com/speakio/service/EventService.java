package br.com.speakio.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.speakio.dto.EventDTO;
import br.com.speakio.exception.EventNotFoundException;
import br.com.speakio.mapper.EventMapper;
import br.com.speakio.model.Event;
import br.com.speakio.repository.EventRepository;
import br.com.speakio.request.EventRequest;

@Service
public class EventService {

	@Autowired
	private EventRepository repository;
	@Autowired
	private EventMapper mapper;

	@Transactional
	public EventDTO save(EventRequest eventRequest) {
		Event event = mapper.requestToModel(eventRequest);
		
	    return mapper.modelToDTO( repository.save(event) );		
	}

	@Transactional
	public EventDTO update(Event event) {
		return mapper.modelToDTO( repository.save(event) );		
	}
	
	public Optional<Event> find(Long id) {
		return repository.findById(id);
	}

	@Transactional
	public void delete(Long id) {
		
		try {
			repository.deleteById(id);
			repository.flush();
		
		} catch (EmptyResultDataAccessException e) {
			throw new EventNotFoundException(id);
		};			
	}

	public List<EventDTO> listAll() {
		
		return repository.findAll()
				.stream()
				.map(event -> mapper.modelToDTO(event))
				.collect(Collectors.toList());	
	}
	
}