package br.com.speakio.service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.speakio.dto.PlataformDTO;
import br.com.speakio.exception.PlataformNotFoundException;
import br.com.speakio.mapper.PlataformMapper;
import br.com.speakio.model.Plataform;
import br.com.speakio.repository.PlataformRepository;
import br.com.speakio.request.PlataformRequest;

@Service
public class PlataformService {
	
	@Autowired
	private PlataformRepository repository;
	
	@Autowired
	private PlataformMapper mapper;
	
	@Transactional
	public PlataformDTO save(PlataformRequest plataformRequest) {
		Plataform plataform = mapper.requestToModel(plataformRequest);
		//cliente.setDataNasc(LocalDate.now());
	    return mapper.modelToDTO( repository.save(plataform) );		
	}
	
	@Transactional
	public PlataformDTO update(Plataform plataform) {
		return mapper.modelToDTO( repository.save(plataform) );		
	}
	
	public Optional<Plataform> find(Long id) {
		return repository.findById(id);
	}
	
	@Transactional
	public void delete(Long id) {
		
		try {
			repository.deleteById(id);
			repository.flush();
		
		} catch (EmptyResultDataAccessException e) {
			throw new PlataformNotFoundException(id);
		};			
	}
	
	public List<PlataformDTO> listAll() {
		
		return repository.findAll()
				.stream()
				.map(plataform -> mapper.modelToDTO(plataform))
				.collect(Collectors.toList());	
	}
	
}