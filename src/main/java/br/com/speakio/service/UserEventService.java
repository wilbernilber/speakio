package br.com.speakio.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.speakio.dto.UserEventDTO;
import br.com.speakio.exception.UserEventNotFoundException;
import br.com.speakio.mapper.UserEventMapper;
import br.com.speakio.model.UserEvent;
import br.com.speakio.repository.UserEventRepository;
import br.com.speakio.request.UserEventRequest;

@Service
public class UserEventService {

	@Autowired
	private UserEventRepository repository;
	@Autowired
	private UserEventMapper mapper;

	@Transactional
	public UserEventDTO save(UserEventRequest UserEventRequest) {
		UserEvent UserEvent = mapper.requestToModel(UserEventRequest);
		//cliente.setDate(LocalDate.now());
	    return mapper.modelToDTO( repository.save(UserEvent) );		
	}

	@Transactional
	public UserEventDTO update(UserEvent UserEvent) {
		return mapper.modelToDTO( repository.save(UserEvent) );		
	}
	
	public Optional<UserEvent> find(Long id) {
		return repository.findById(id);
	}

	@Transactional
	public void delete(Long id) {
		
		try {
			repository.deleteById(id);
			repository.flush();
		
		} catch (EmptyResultDataAccessException e) {
			throw new UserEventNotFoundException(id);
		};			
	}

	public List<UserEventDTO> listAll() {
		
		return repository.findAll()
				.stream()
				.map(UserEvent -> mapper.modelToDTO(UserEvent))
				.collect(Collectors.toList());	
	}
	
}
