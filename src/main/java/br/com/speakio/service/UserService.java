package br.com.speakio.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.speakio.dto.UserDTO;
import br.com.speakio.exception.UserNotFoundException;
import br.com.speakio.mapper.UserMapper;
import br.com.speakio.model.User;
import br.com.speakio.repository.GroupUserRepository;
import br.com.speakio.repository.UserRepository;
import br.com.speakio.request.UserRequest;

@Service
public class UserService {

	@Autowired
	private UserRepository repository;
	@Autowired
	private GroupUserRepository groupUserRepository;
	@Autowired
	private UserMapper mapper;
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Transactional
	public UserDTO save(UserRequest userRequest) {
		User user = mapper.requestToModel(userRequest);
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		user.setGroups(groupUserRepository.findGroup(user.getAccess()));
		
	    return mapper.modelToDTO( repository.save(user) );		
	}

	@Transactional
	public UserDTO update(User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		return mapper.modelToDTO( repository.save(user) );		
	}
	
	public Optional<User> find(Long id) {
		return repository.findById(id);
	}

	@Transactional
	public void delete(Long id) {
		
		try {
			repository.deleteById(id);
			repository.flush();
		
		} catch (EmptyResultDataAccessException e) {
			throw new UserNotFoundException(id);
		};			
	}

	public List<UserDTO> listAll() {
		
		return repository.findAll()
				.stream()
				.map(user -> mapper.modelToDTO(user))
				.collect(Collectors.toList());	
	}
	
}