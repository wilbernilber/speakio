package br.com.speakio;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CategoryIT {

	@LocalServerPort
	private int port;
	
	@Before
	public void setUp() {
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
		RestAssured.port = port;
		RestAssured.basePath = "categories";	
	}
	
	@Test
	public void shouldReturnStatus200_WhenToConsultTheCategory() {
				
		RestAssured.given()
			.accept(ContentType.JSON)	
			.header("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lX3VzZXIiOiJzcGVha2lvMSIsInVzdWFyaW9faWQiOjEsInVzZXJfbmFtZSI6InNwZWFraW8iLCJzY29wZSI6WyJ3cml0ZSIsInJlYWQiXSwiZXhwIjoxNjA2OTM3Nzc0LCJhdXRob3JpdGllcyI6WyJTUEVBS0lPOTkiLCJTUEVBS0lPMDEiXSwianRpIjoiZDdhMzE4NGQtNTJiOC00ZDM2LTkxYWEtNWU2YTlkMmU2ZTUzIiwiY2xpZW50X2lkIjoiZnJvbnRlbmQtY2xpZW50In0.vZV_e9XhRhAYF8Clf17--aEVnICcv5gwpDygKO2OzHM").when()
			.get("")
		.then()
			.statusCode(200);
	}
	
	@Test
	public void shouldReturnStatus401_WhenToConsultTheCategory() {
				
		RestAssured.given()
			.accept(ContentType.JSON)	
			.header("Authorization", "Bearer 1yJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lX3VzZXIiOiJzcGVha2lvMSIsInVzdWFyaW9faWQiOjEsInVzZXJfbmFtZSI6InNwZWFraW8iLCJzY29wZSI6WyJ3cml0ZSIsInJlYWQiXSwiZXhwIjoxNjA2OTM3Nzc0LCJhdXRob3JpdGllcyI6WyJTUEVBS0lPOTkiLCJTUEVBS0lPMDEiXSwianRpIjoiZDdhMzE4NGQtNTJiOC00ZDM2LTkxYWEtNWU2YTlkMmU2ZTUzIiwiY2xpZW50X2lkIjoiZnJvbnRlbmQtY2xpZW50In0.vZV_e9XhRhAYF8Clf17--aEVnICcv5gwpDygKO2OzHM").when()
			.get("")
		.then()
			.statusCode(401);
	}

}
